import random
import string


def generate_random_code(length: int) -> str:
    """
    Generate a random alphanumeric code of a given length.

    This function generates a random string consisting of ASCII letters and digits.
    The length of the generated string is specified by the 'length' parameter.

    Parameters:
        length (int): the length of the generated code.

    Returns:
        str: a randomly generated alphanumeric code of the specified length.
    """
    return "".join(random.choice(string.ascii_letters + string.digits) for _ in range(length))
