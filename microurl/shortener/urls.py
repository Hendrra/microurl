from django.urls import path

from .api.views import CreateShortURLView, RetrieveURLView

urlpatterns = [
    path("", CreateShortURLView.as_view(), name="create_short_url"),
    path("<str:short_url>/", RetrieveURLView.as_view(), name="redirect_to_url"),
]
