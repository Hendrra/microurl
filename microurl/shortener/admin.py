from django.contrib import admin

from .models import URL


class URLAdmin(admin.ModelAdmin):
    list_display = ("original_url", "short_url")
    search_fields = ("original_url", "short_url")


admin.site.register(URL, URLAdmin)
