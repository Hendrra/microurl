import unittest

from ddt import data, ddt, unpack
from rest_framework.exceptions import ValidationError
from shortener.api.serializers import URLSerializer
from shortener.models import URL


@ddt
class TestURLSerializer(unittest.TestCase):
    @unpack
    @data(
        {
            "original_url": "https://www.example.com",
            "short_url": "a1b2c3",
            "expected": {"original_url": "https://www.example.com", "short_url": "a1b2c3"},
        },
    )
    def test_valid_serializer(self, original_url, short_url, expected):
        """
        Test valid serialization from model instance to dictionary.
        """
        url_instance = URL(original_url=original_url, short_url=short_url)
        serializer = URLSerializer(url_instance)
        self.assertEqual(serializer.data, expected)

    @data(
        {"original_url": "https://www.example.com"},
    )
    def test_valid_deserializer(self, entry):
        """
        Test valid deserialization from dictionary to model instance.
        """
        serializer = URLSerializer(data=entry)
        self.assertTrue(serializer.is_valid())

        url_instance = serializer.save()
        self.assertEqual(url_instance.original_url, entry["original_url"])
        self.assertIsNotNone(url_instance.short_url)

    @data(
        {"original_url": ""},
    )
    def test_invalid_deserializer(self, entry):
        """
        Test invalid deserialization and validation.
        """
        serializer = URLSerializer(data=entry)
        self.assertFalse(serializer.is_valid())

        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)
