from unittest import mock

from ddt import data, ddt
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from shortener.config import CREATE_DOMAIN, MAX_SHORT_URL_GENERATION_ATTEMPTS
from shortener.models import URL


@ddt
class TestCreateShortURLView(TestCase):
    URL = reverse("create_short_url")

    @data(
        {"original_url": "https://www.example.com"},
    )
    def test_create_short_url_success(self, entry):
        """
        Test successful URL shortening.
        """
        response = self.client.post(self.URL, entry, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        computed = response.data

        self.assertIn("short_url", computed)
        self.assertIn(CREATE_DOMAIN, computed["short_url"])

    @mock.patch("shortener.api.views.URL.objects.all")
    @mock.patch("shortener.api.views.generate_random_code")
    @data(
        {"original_url": "https://www.example.com"},
    )
    def test_create_short_url_limit(self, entry, mock_generate, mock_all):
        """
        Test the limit for URL shortening.
        """
        mock_generate.return_value = "a"
        mock_all.return_value.values_list.return_value = ["a"]
        response = self.client.post(self.URL, entry, format="json")

        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

        computed = response.data

        self.assertIn("error", computed)
        self.assertIn(
            f"Could not generate a unique short URL after {MAX_SHORT_URL_GENERATION_ATTEMPTS} attempts.",
            computed["error"],
        )

    def test_create_short_url_api_redirect(self):
        """
        Test error when trying to shorten the API's own URL.
        """
        entry = {"original_url": CREATE_DOMAIN + reverse("create_short_url")}
        response = self.client.post(self.URL, entry, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        computed = response.data

        self.assertIn("error", computed)
        self.assertIn("You cannot shorten the URL of the API itself.", computed["error"])


class TestRetrieveURLView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url_obj = URL.objects.create(original_url="https://www.example.com", short_url="a1b2c3")

    def test_retrieve_url_success(self):
        """
        Test successful URL retrieval.
        """
        url = reverse("redirect_to_url", kwargs={"short_url": "a1b2c3"})
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["original_url"], "https://www.example.com")
