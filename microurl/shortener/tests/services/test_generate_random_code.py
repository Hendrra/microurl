import string
import unittest

from shortener.services import generate_random_code


class TestGenerateRandomCode(unittest.TestCase):
    LENGTH = 6

    def test_length(self):
        """
        Test if the generated code has the correct length.
        """
        code = generate_random_code(self.LENGTH)
        self.assertEqual(len(code), self.LENGTH)

    def test_alphanumeric(self):
        """
        Test if the generated code is alphanumeric.
        """
        code = generate_random_code(self.LENGTH)
        self.assertTrue(all(c in string.ascii_letters + string.digits for c in code))

    def test_randomness(self):
        """
        Test if the generated code is different each time.
        In fact this test can fail, but the probability of failing is ultra low ~ 1 : 62 ** 6.
        """
        code1 = generate_random_code(self.LENGTH)
        code2 = generate_random_code(self.LENGTH)
        self.assertNotEqual(code1, code2)
