from rest_framework import generics, status
from rest_framework.request import Request
from rest_framework.response import Response

from ..config import CREATE_DOMAIN, MAX_SHORT_URL_GENERATION_ATTEMPTS, RETRIEVE_DOMAIN
from ..models import URL
from ..services import generate_random_code
from .serializers import URLSerializer


class CreateShortURLView(generics.CreateAPIView):
    """
    API endpoint for creating shortened URLs.

    This view takes an original URL and returns a shortened URL.
    """

    queryset = URL.objects.all()
    serializer_class = URLSerializer

    def create(self, request: Request, *args, **kwargs) -> Response:
        """
        Create a new shortened URL.

        Validates the incoming request data, generates a unique short URL,
        and returns the shortened URL in the response.

        It ensures that the shortened URL is unique and does not match the API's own URL.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        original_url = serializer.validated_data["original_url"]

        # check to ensure that there will be no redirection error
        if CREATE_DOMAIN in original_url:
            return Response(
                {"error": "You cannot shorten the URL of the API itself."}, status=status.HTTP_403_FORBIDDEN
            )

        # loop to ensure that the short_url is unique
        attempt = 0
        short_url_is_unique = False

        current_short_urls = URL.objects.all().values_list("short_url", flat=True)

        while not short_url_is_unique:
            short_url = generate_random_code(length=6)
            short_url_is_unique = short_url not in current_short_urls

            attempt += 1
            if attempt >= MAX_SHORT_URL_GENERATION_ATTEMPTS:
                return Response(
                    {
                        "error": "Could not generate a unique short "
                        f"URL after {MAX_SHORT_URL_GENERATION_ATTEMPTS} attempts."
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                )

        URL.objects.create(original_url=original_url, short_url=short_url)
        return Response({"short_url": RETRIEVE_DOMAIN.format(short_url=short_url)}, status=status.HTTP_201_CREATED)


class RetrieveURLView(generics.RetrieveAPIView):
    """
    API endpoint for retrieving the original URL from a shortened URL.

    This view takes a shortened URL and returns the corresponding original URL with the six character unique code
    linked to that URL.
    """

    lookup_field = "short_url"
    queryset = URL.objects.all()
    serializer_class = URLSerializer
