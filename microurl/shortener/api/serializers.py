from rest_framework import serializers

from ..models import URL


class URLSerializer(serializers.ModelSerializer):
    """
    Serializer for the URL model.

    It specifies that the 'short_url' field is read-only, meaning it will be included
    in serialized output but won't be accepted as an input during deserialization.
    """

    short_url = serializers.CharField(read_only=True)

    class Meta:
        model = URL
        fields = ("original_url", "short_url")
