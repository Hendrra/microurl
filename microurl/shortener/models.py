from django.db import models

from .services import generate_random_code


class URL(models.Model):
    """
    URL Model for storing original and shortened URLs.

    Fields:
        original_url (models.URLField): the original URL to be shortened,
        short_url (models.CharField): the shortened URL code, unique for each original URL.
    """

    original_url = models.URLField()
    short_url = models.CharField(max_length=6, unique=True, default=generate_random_code(length=6))

    def __str__(self) -> str:
        return f"{self.original_url} ({self.short_url})"
