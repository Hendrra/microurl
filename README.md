# MicroURL

## Description

MicroURL is a URL shortening service built with Django and Django REST Framework. It allows users to shorten long URLs, by creating a unique six-character code, and retrieve the original URLs using the shortened versions. The solution is almost as minimalistic as it can be.

## Features

- Shorten long URLs
- Retrieve original URLs using short codes
- RESTful API for URL shortening and retrieval

## Prerequisites

- Python 3.11 or higher
- Poetry package manager

## Setup

### Clone the Repository and navigate to the source directory

```bash
git clone https://gitlab.com/Hendrra/microurl.git
```

```bash
cd microurl
```

### Install Dependencies

We use Poetry for dependency management. Install all the required packages with:

```bash
poetry install
```

### Database Setup

Run the following command to apply the database migrations:

```bash
python microurl/manage.py migrate
```

## Running the Application

To run the development server, execute:

```bash
python microurl/manage.py runserver
```

The application will be accessible at `http://localhost:8000/`.

## Running Tests

To run the test suite, execute:

```bash
python microurl/manage.py test shortener
```

## API Endpoints

- Create Short URL: `POST /`
- Retrieve Original URL: `GET /{short_url}/`

# Solution

## Implemented Solution

The current implementation focuses on providing a straightforward and efficient way to shorten URLs without any expiration or deletion. Moreover, the URLS are not unique, i.e. if two users create a short URL for the same link they are likely to get two, different shorten URLs.

### Technical Details
- **Unique short URLs**: each original URL is converted into a unique short URL using a random code generator.
- **Code structure**: the generated code for the original URL consists of 6-characters, which can be upper and lower case letters or numbers. 
- **No expiration**: short URLs do not have an expiration time and will persist in the database indefinitely unless manually deleted.
- **Uniqueness check**: a loop ensures that the generated short URL is unique by checking against existing entries in the database.
- **Error handling**: the system returns an error if it cannot generate a unique short URL after a certain number of attempts or if someone tries to shorten the URL of the API itself (redirection).

### Workflow

- **URL shortening**: when a new URL is shortened, its original URL and the generated short URL are stored in the database.
- **URL retrieval**: users can retrieve the original URL by accessing the short URL. This is possible multiple times without any time restrictions.

This implementation offers a simple and effective way to shorten URLs, prioritizing ease of use and persistence over features like expiration or one-time use.

### Pros and Cons

#### Pros
- **Simplicity**: the implementation is straightforward, making it easy to understand and maintain.
- **No time limit**: short URLs do not expire, allowing them to be used indefinitely.
- **Multiple uses**: users can use the short URL multiple times without any restrictions.
- **Immediate availability**: short URLs are available for use as soon as they are created.

#### Cons
- **Database growth**: since URLs are stored indefinitely, the database could grow large over time, potentially affecting performance.
- **Lack of security**: the absence of expiration makes the short URLs less secure for sensitive information.
- **No automatic cleanup**: the database could become cluttered with old and unused URLs, as there is no automatic deletion mechanism.

## Design Considerations

### Uniqueness of `original_url`

One of the design decisions to consider is whether the `original_url` field should be unique. Making it unique would prevent duplicate entries in the database for the same original URL, thereby saving storage space. However, it would also mean that multiple users cannot shorten the same URL independently.

#### Pros
- Saves database storage.
- Simplifies the logic for URL retrieval.

#### Cons
- Multiple users cannot have different short URLs for the same original URL.
- Additional logic needed to handle the case where a URL is already shortened.

### Deleting Short URLs After Retrieval

Another design aspect to consider is whether to delete the short URL from the database once it has been used to retrieve the original URL.

#### Pros
- Enhances security by making the short URL a one-time use token.
- Saves database storage over time.

#### Cons
- The same short URL cannot be used multiple times, reducing the utility of the service for some use-cases.
- Additional logic needed to handle deletion and potential re-creation of short URLs.

### Hybrid Approach: Scheduled Deletion with `created_at`

A balanced solution to the design considerations is to implement a hybrid approach that combines the benefits of both keeping and deleting short URLs. In this method, a `created_at` field is added to the `URL` model to track when each short URL is created. A scheduled task, managed by RabbitMQ and Celery, is then set to delete each short URL after a certain amount of time (e.g. 24 hours) after its creation.

#### Pros
- Allows the short URL to be used multiple times within a certain time window, enhancing the utility of the service.
- Ensures that old URLs are automatically deleted, saving database storage over time.
- Adds a layer of security by making the short URL temporary, though not a one-time use token.

#### Cons
- Requires the implementation of RabbitMQ and Celery, adding complexity to both development and deployment.
- The task queue will consume additional server resources.
- The expiration time could be limiting for users who need the short URL to persist for a longer period.
